FROM ubuntu:latest

WORKDIR /app

COPY app.py /app

COPY static /app/static

COPY templates /app/templates

RUN apt update

RUN apt install -y python3

RUN apt install -y python3-pip

RUN pip install Flask

EXPOSE 5000

CMD ["python3", "app.py"]